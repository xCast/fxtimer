/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx_timer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JuanP
 */
public class FX_TimeHanndler implements Runnable
{
    public double currentTime, startTime, timeStart;
    public boolean stop;
    private InterfaceController theInterface;

    public FX_TimeHanndler(InterfaceController theInterface)
    {
        this.theInterface = theInterface;
        reset();
    }

    public void reset()
    {
        currentTime = 0;
        timeStart = 0;
        startTime = System.currentTimeMillis();
        stop = false;
    }
    
    public void action()
    {
        timeStart = System.currentTimeMillis();
    }

    private double getCurrentTime()
    {
        return System.currentTimeMillis() - startTime;
    }
    
    @Override
    public void run()
    {
        while (!stop)
        {
            //System.out.println("Time = "+getCurrentTime());
            theInterface.setInterfaceData(getCurrentTime() + "",System.currentTimeMillis()-timeStart+"");
            try
            {
                Thread.sleep(100);
            } catch (InterruptedException ex)
            {
                Logger.getLogger(TimeHanndler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
