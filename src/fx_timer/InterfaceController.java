/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx_timer;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXSpinner;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


/**
 * FXML Controller class
 *
 * @author JuanP
 */
public class InterfaceController implements Initializable
{
    @FXML private Label lb_CurTime;
    @FXML private Label lb_counterFrom;
    @FXML private Label lb_counterTotal;
    @FXML private Label label111;
    @FXML private Label label;
    @FXML private JFXButton btn_Reset;
    @FXML private AnchorPane mainPane;
    @FXML private JFXButton btn_Action;
    @FXML private Label label1;
    @FXML private Label lb_TimeFrom;
    @FXML private Label label11;
    @FXML private JFXSpinner spinner;
    @FXML private JFXProgressBar progressBar;
    @FXML private Stage stage;
    private TimeHanndler timer;
    private int counter = 0;
    private int totalCounter = 0;
    private int maxCounter = -1;
    private double curTime,timeFrom;
    
    
    @FXML private void handdleButton(ActionEvent e)
    {
         EventTarget target =  e.getTarget();
         if(target == btn_Action)
         {
            timer.action();
            totalCounter++;
            if(maxCounter<timer.getLastClics()) {maxCounter=timer.getLastClics();}
         }
         else if(target == btn_Reset)
         {
            timer.reset();
            counter = 0;
         }
    }
    
    public void setStage(Stage st)
    {
        this.stage = st;
        this.stage.setOnCloseRequest(new EventHandler<WindowEvent>()
        {
            @Override
            public void handle(WindowEvent t)
            {
                timer.stop = true;
                System.out.println("CLOSING");
                System.exit(0);
            }
        });
    }
    
    public void incCounter() {counter++;}
    
    public void setInterfaceData(String curTime,String timeFrom)
    {
        lb_CurTime.setText(curTime);
        lb_TimeFrom.setText(timeFrom);
        lb_counterTotal.setText(totalCounter+"");
        lb_counterFrom.setText(timer.getLastClics()+"");
        progressBar.setProgress((double)(timer.getLastClics())/maxCounter);
        spinner.setProgress((double)(timer.getLastClics())/maxCounter);
        
        //System.out.println("maxCounter = "+maxCounter+" LastClics = "+timer.getLastClics()+
        //        " Setting progress: "+((double)timer.getLastClics())/maxCounter);
    }
        
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        System.setProperty("prism.lcdtext", "false");
        //spinner.setSkin(new JFXSpinnerSkinCustom(spinner));
        timer = new TimeHanndler(this);
        timer.start();
        //timer = new FX_TimeHanndler(this);
        //Platform.runLater(new FX_TimeHanndler(this));
    }    
    
}
